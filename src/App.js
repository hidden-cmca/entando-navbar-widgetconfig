import React, { useState } from "react";
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { navSpec: ''};
  }

  handleNavSpecChange(value) {
    this.setState(prevState => ({
      ...prevState,
      navSpec: value,
    }));
  }

  render() {
    const { navSpec: value } = this.state;
    return (
      <div>
        <h1>navSec Widget Configuration</h1>
        <h3>Configurazione pagine menu (tipo code(codicePagina1) + code(codicePagina2))</h3>
        <label htmlFor="name">navSpec</label>
        <input style={{marginLeft:"7px", width:"70%"}} id="name" onChange={e => this.handleNavSpecChange(e.target.value)} defaultValue={value} />
      </div>
    );
  }
}

export default App;
 
